function createCard( title, description, pictureUrl, starts, ends, subtitle) {
  return `
  <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle">${subtitle}</h6>
        <p class ="card-text>${description}<p/>
        <div class ='footer'>
         ${dateFormatter(starts)} - ${dateFormatter(ends)}
         </div>
      </div>
    </div>
  `;
}


function dateFormatter(datetime){
  const date = new Date(datetime);
  const month = date.getMonth()+1;
  const day = date.getDate();
  const year = date.getFullYear();
  return `${month}/${day}/${year}`;
}

function problemError(alert){
  const html= `<div class="alert alert-primary" role="alert">
  alert = ${alert}
</div>`
  const error = document.querySelector('.error');
  error.innerHTML = html

}



window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("An ERROR occured");
      problemError("Response was not OK")

      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();


      let count  = 0;
      const columns = document.querySelectorAll('.col');
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const subtitle = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl, starts, ends, subtitle);
          const column = columns[count % columns.length]
          column.innerHTML += html;
          console.log(details);
          count += 1

        }
      }

    }
  } catch (e) {
    console.error ("unable to grab data", e);
    problemError(e);
    // Figure out what to do if an error is raised
  }

});
