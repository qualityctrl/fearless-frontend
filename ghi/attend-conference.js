function createElement() {
    return `
    <div class="d-flex justify-content-center mb-3" id="loading-conference-spinner">
    <div class="spinner-grow text-secondary" role="status">
      <span class="visually-hidden">Loading...</span>
    </div>
  </div>
  <div class="mb-3">
    <select name="conference" id="conference" class="form-select d-none" required>
      <option value="">Choose a conference</option>
    </select>
  </div>
    `;
}








window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-icon');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        const html = createElement(selectTag)
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);

    }

    // Hide the loading icon
    loadingIcon.classList.add('d-none');

    // Show the select tag
    selectTag.classList.remove('d-none');
  }

});
